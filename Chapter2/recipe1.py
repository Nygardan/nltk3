# pp30-32: Stemming words

from nltk.stem import PorterStemmer, LancasterStemmer, RegexpStemmer, SnowballStemmer

# Set up stemmers - note the regexp and snowball parameters
portstem = PorterStemmer()
lancstem = LancasterStemmer()
regstem = RegexpStemmer('ing$|e$|s$|able$', min=4)
snowstem = SnowballStemmer('english')

print("Enter a word to be stemmed:")
word = input()

# Print our stemmed word in four types
print(f"PorterStemmer: {portstem.stem(word)}")
print(f"LancasterStemmer: {lancstem.stem(word)}")
print(f"RegexpStemmer: {regstem.stem(word)}")
print(f"SnowballStemmer: {snowstem.stem(word)}")