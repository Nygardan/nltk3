# pp. 43-45: Replacing synonyms
# Skipped using the csv example from the book - used yaml only

from replacers import YamlWordReplacer
from nltk.tokenize import RegexpTokenizer

tokenizer = RegexpTokenizer("[\w']+")

# New synonyms can be added to synonyms.yaml
replacer = YamlWordReplacer('synonyms.yaml')

print("Enter a short phrase containing 'serpent' or 'hound':")
phrase = input()

print(f"Phrase: {phrase}")
print("Phrase, tokenized, with synonyms replaced:")

word_list = tokenizer.tokenize(phrase)

[print(replacer.replace(word)) for word in word_list]
