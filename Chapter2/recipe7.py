# pp 46-48: Replacing negations with antonyms
# This uses my modification of AntonymReplacer (see in replacers.py)

from replacers import AntonymReplacer
from nltk.tokenize import RegexpTokenizer

print("Enter a phrase containing a negation using 'not':")
phrase = input()

tokenizer = RegexpTokenizer("[\w']+")
replacer = AntonymReplacer()

word_list = tokenizer.tokenize(phrase)
print("Phrase with negation replaced:")
print(f"{replacer.replace_negations(word_list)}")