# pp 32-34: Lemmatizing words with WordNet

from nltk.stem import WordNetLemmatizer

# A lemma is a root word, as opposed to a root stem.
lemmatizer = WordNetLemmatizer()

print("Enter a word:")
word = input()

print("What kind of word?\n1) Noun\n2) Verb\n3) Adjective\n4) Adverb")
pos = input()

if pos == '1':
    print(f"Noun lemma of {word}: {lemmatizer.lemmatize(word, pos='n')}")
elif pos == '2':
    print(f"Verb lemma of {word}: {lemmatizer.lemmatize(word, pos='v')}")
elif pos == '3':
    print(f"Adjective lemma of {word}: {lemmatizer.lemmatize(word, pos='a')}")
elif pos == '4':
    print(f"Adverb lemma of {word}: {lemmatizer.lemmatize(word, pos='r')}")
else:
    print("Incorrect part-of-speech input. Exiting.")
    exit(0)