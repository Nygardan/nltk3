# pp 37-39: Removing repeating characters

from replacers import RepeatReplacer

replacer = RepeatReplacer()

print("Enter a word with repeating letters:")
word = input()

print(replacer.replace(word))