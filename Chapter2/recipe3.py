# pp. 34-37: Replacing words matching regular expressions

from replacers import RegexpReplacer
from nltk.tokenize import word_tokenize

replacer = RegexpReplacer()

print("Enter a phrase containing a contraction:")
phrase = input()

# Use replacer to "fix" before tokenizing
token_list = word_tokenize(replacer.replace(phrase))

print("The tokenized phrase:")

[print(token, end=" ") for token in token_list]
