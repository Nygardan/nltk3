# pp 39-42: Spelling correction with Enchant

from replacers import SpellingReplacer
from nltk.tokenize import RegexpTokenizer

tokenizer = RegexpTokenizer("[\w']+")
replacer = SpellingReplacer()

print("Enter a short phrase for spell check:")
phrase = input()

print(f"Phrase: {phrase}")
print("Phrase spell-checked:")

word_list = tokenizer.tokenize(phrase)

[print(replacer.replace(word)) for word in word_list]

