# page 14-16 - Training a sentence tokenizer

from nltk.tokenize import PunktSentenceTokenizer
from nltk.corpus import webtext
from nltk.tokenize import sent_tokenize

# Training the Punkt tokenizer and using it
text = webtext.raw('overheard.txt')
sent_tokenizer = PunktSentenceTokenizer(text)

sents1 = sent_tokenizer.tokenize(text)
print(sents1[0])

# Using the untrained tokenizer
sents2 = sent_tokenize(text)
print(sents2[0])
print(sents1[678])
print(sents2[678])