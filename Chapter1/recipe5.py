# pp 16-18: Filtering stopwords in a tokenized sentence

from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer

# Tokenizes input on white space using regular expressions.
tokenizer = RegexpTokenizer('\s+', gaps=True)

# Sets our stop words (English).
english_stops = set(stopwords.words('english'))

# Gets input, tokenizes it, then prints.
print("Enter a sentence containing stopwords:")
sentence = input()
words = tokenizer.tokenize(sentence)

# Note 'if word.lower()' - stopwords is case-sensitive.
print([word for word in words if word.lower() not in english_stops])