# pp 25-27: Discovering word collocations.

from nltk.corpus import webtext, stopwords
from nltk.collocations import BigramCollocationFinder, TrigramCollocationFinder
from nltk.metrics import BigramAssocMeasures, TrigramAssocMeasures

# pull text from Monty Python
words = [w.lower() for w in webtext.words('grail.txt')]

# set the Bigram Collocation Finder to look at the Monty Python words
bcf = BigramCollocationFinder.from_words(words)

# print an evaluation of the text with no filter
print(bcf.nbest(BigramAssocMeasures.likelihood_ratio, 6))

# set stops and filter out words less than three letters
stopset = set(stopwords.words('english'))
filter_stops = lambda w: len(w) < 3 or w in stopset
bcf.apply_word_filter(filter_stops)
print(bcf.nbest(BigramAssocMeasures.likelihood_ratio, 6))

# print the collocations from pirates.txt
words = [w.lower() for w in webtext.words('pirates.txt')]
bcf = BigramCollocationFinder.from_words(words)
bcf.apply_word_filter(filter_stops)
print("\nNew bigram list from Pirates of the Caribbean:")
print(bcf.nbest(BigramAssocMeasures.likelihood_ratio, 6))

# trigram collocation
tcf = TrigramCollocationFinder.from_words(words)
tcf.apply_word_filter(filter_stops)

# filter out any collocations that appear < 3 times
tcf.apply_freq_filter(3)
print("\nNew trigram list from Pirates of the Caribbean:")
print(tcf.nbest(TrigramAssocMeasures.likelihood_ratio, 4))