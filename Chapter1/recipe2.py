# Tokenizes input into words, separates punctuation into separate tokens.

import nltk
from nltk.tokenize import word_tokenize

print("Enter a sentence: ")
sentence = input()
print(word_tokenize(sentence))