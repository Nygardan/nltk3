import nltk
from nltk.tokenize import sent_tokenize

para = "Hello world. It's good to see you. Thanks for buying this book."
tokens = sent_tokenize(para)
print(tokens)