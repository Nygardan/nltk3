# Tokenizes input into words, uses regular expressions.

import nltk
from nltk.tokenize import RegexpTokenizer

# Keeps contractions together as one token.
tokenizer = RegexpTokenizer("[\w']+")

print("Enter a sentence: ")
sentence = input()
print(tokenizer.tokenize(sentence))

# Separates tokens on white space
tokenizer = RegexpTokenizer('\s', gaps=True)
print("Enter another sentence: ")
sentence = input()
print(tokenizer.tokenize(sentence))