# pp 18-20: Looking up Synsets for a word in WordNet

from nltk.corpus import wordnet
import random

print("Enter a word:")
word = input()

synset = wordnet.synsets(word)

# end the program if no synsets, otherwise show definitions, hypernyms, and hyponyms
if len(synset) == 0:
    print("No synsets for this word.")
else:
    print(f"Synsets for {word}:")
    print(synset)

    for syn in synset:
        print(syn.definition())

    r1 = random.randint(0, (len(synset) - 1))
    print(f"Hypernyms and Hyponyms for {synset[r1]}:")
    print("Hypernyms:")
    print(synset[r1].hypernyms())
    print("Hyponyms:")
    print(synset[r1].hyponyms())