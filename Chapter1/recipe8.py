# pp. 23-25: Calculating WordNet Synset similarity.

from nltk.corpus import wordnet

print("Enter a word:")
word1 = input()
print("Enter another word:")
word2 = input()

for syn in wordnet.synsets(word1):
    for s in wordnet.synsets(word2):
        print(f"{syn} vs. {s} similarity: {syn.wup_similarity(s)}")
print("")