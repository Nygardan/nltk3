# pp 20-22: Looking up lemmas and synonyms in WordNet

from nltk.corpus import wordnet
print("Enter a word:")
word = input()
synset = wordnet.synsets(word)

# Check for validity of input
if len(synset) == 0:
    print("There are no synsets for this word.")
    exit()

# Display the name and definition of all lemmas in the synset
for syn in synset:
    lemmas = syn.lemmas()
    for lemma in lemmas:
        print(f"{lemma.name()}: {lemma.synset().definition()}")

# Print all synonyms with no duplicates
print(f"\nAll synonyms of {word}:")
synonyms = []
for w in wordnet.synsets(word):
    for lemma in w.lemmas():
        synonyms.append(lemma.name())

# Use set() to remove duplicates
for s in set(synonyms):
    print(s, end=" ")
print("\n")

# Gather and print all antonyms and their definitions.
print(f"\nAll antonyms within the synset {word}:")
antonyms = []
for w in wordnet.synsets(word):
    for lemma in w.lemmas():
        # I'm just trying list comprehensions out here.
        [antonyms.append(ant) for ant in lemma.antonyms()]

# Here's another list comprehension to print the name/definition.
[print(f"{ant.name()}: {ant.synset().definition()}") for ant in antonyms]




