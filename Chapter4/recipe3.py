# pp. 92-93: Combining taggers with backoff tagging.
# Very similar to recipe 2 but uses a default tagger so that unknown words get 'NN'

from nltk.tag import UnigramTagger, DefaultTagger
from nltk.corpus import treebank
from nltk.tokenize import word_tokenize

train_sents = treebank.tagged_sents()[:3000]
default_tagger = DefaultTagger('NN')
tagger = UnigramTagger(train_sents, backoff=default_tagger)

print("Enter a phrase: ")
phrase = word_tokenize(input())

print(tagger.tag(phrase))