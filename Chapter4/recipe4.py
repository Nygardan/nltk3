# pp. 94-97: Training and combining ngram taggers

from nltk.tag import BigramTagger, TrigramTagger, DefaultTagger, UnigramTagger
from nltk.corpus import treebank
from tag_util import backoff_tagger
from nltk.tokenize import word_tokenize

train_sents = treebank.tagged_sents()[:3000]
backoff = DefaultTagger('NN')

# Tries to find context for applying tags. Not quite perfect!
tagger = backoff_tagger(train_sents, [UnigramTagger, BigramTagger, TrigramTagger], backoff=backoff)

print("Enter a phrase: ")
phrase = word_tokenize(input())

print(tagger.tag(phrase))