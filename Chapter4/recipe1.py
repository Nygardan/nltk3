# pp.86-89: Default tagging 
# This program evaluates the accuracy of a noun, verb, and adjective default tagger


from nltk.tag import DefaultTagger
from nltk.corpus import treebank

noun_tagger = DefaultTagger('NN')
verb_tagger = DefaultTagger('VB')
adj_tagger = DefaultTagger('JJ')

test_sents = treebank.tagged_sents()[3000:]

print(f"Noun accuracy: {noun_tagger.evaluate(test_sents)}")
print(f"Verb accuracy: {verb_tagger.evaluate(test_sents)}")
print(f"Adjective accuracy: {adj_tagger.evaluate(test_sents)}")