# pp. 89-92: Training a unigram part-of-speech tagger

from nltk.tag import UnigramTagger
from nltk.corpus import treebank
from nltk.tokenize import word_tokenize

train_sents = treebank.tagged_sents()[:3000]
tagger = UnigramTagger(train_sents)

print("Enter a phrase: ")
phrase = word_tokenize(input())

print(tagger.tag(phrase))